import { handleActions } from 'redux-actions';

const INITIAL_STATE = {
  todos: [],
};

const TodosReducer = handleActions(
  {
    GET_ALL_TODOS: (state, payload) => {
      return {
        ...state,
        todos: payload.todos,
      };
    },
  },
  INITIAL_STATE
);

export default TodosReducer;
