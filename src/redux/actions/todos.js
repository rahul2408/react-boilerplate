import { createAction } from 'redux-actions';
import { getEndpoint } from '../../constants/apiEndpoints';
import axios from '../../utils/axios';

export const GET_ALL_TODOS = createAction('GET_ALL_TODOS');

const todosEndpoint = getEndpoint('todos');

export const getAllTodos = () => async (dispatch) => {
  try {
    const todos = await axios.get(`${todosEndpoint}`);
    dispatch({
      type: GET_ALL_TODOS,
      todos: todos.data,
    });
  } catch (err) {
    console.log(err);
    console.error('Unable to get all todos');
  }
};
