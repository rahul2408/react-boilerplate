import axios from 'axios';

import { apiEndpoint } from '../constants/apiEndpoints';

export default axios.create({
  baseURL: apiEndpoint,
  headers: { Accept: 'application/json' },
});
