import React, { useEffect } from 'react';

import './styles.scss';

const Todos = ({ getAllTodos, todos }) => {
  useEffect(() => {
    getAllTodos();
  }, []);

  useEffect(() => {
    if (todos) console.log('Todos Fetched', todos);
  }, [todos]);

  return <div>Check console now!</div>;
};

export default Todos;
