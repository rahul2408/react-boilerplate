import React from 'react';
import { connect } from 'react-redux';

import Todos from '../../components/Todos';

import { getAllTodos } from '../../redux/actions/todos';

const Home = ({ getAllTodos, todos }) => {
  return <Todos getAllTodos={getAllTodos} todos={todos} />;
};

const mapStateToProps = ({ todos }) => ({
  todos: todos.todos,
});

export default connect(mapStateToProps, {
  getAllTodos,
})(Home);
