export const apiEndpoint = process.env.REACT_APP_API_URL || 'http://localhost';
export const apiEnvironment =
  process.env.REACT_APP_ENVIRONMENT || 'development';

const apiConfig = {
  production: {
    todos: `${apiEndpoint}/todos`,
  },
  development: {
    todos: `${apiEndpoint}/todos`,
  },
};

export const getEndpoint = (service) => apiConfig[apiEnvironment][service];
